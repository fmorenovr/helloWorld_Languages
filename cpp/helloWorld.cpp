#include <iostream>

using namespace std;

int main(int argc,char* argv[]){
  int i;
  cout<<endl<<"\tHola Mundo C++ !!"<<endl<<endl;
  cout<<"Numero de argumentos: "<<argc<<endl;
  for(i=0;i<argc;i++){
    cout<<"Argumento["<<i<<"] = "<<argv[i]<<endl;
  }
  return 0;
}

// run the file:
// g++ helloWorld.c++ -o helloWorld
// ./helloWorld

#!/bin/bash

#[[ $a == z* ]]   # Verdadero si $a empieza con una "z" (expresión regular coincide).
#[[ $a == "z*" ]] # Verdadero si $a es igual a z* (coincide literalmente).

#[ $a == z* ]     # Ocurre división de palabras.
#[ "$a" == "z*" ] # Verdadero si $a es igual a z* (coincide literalmente).

#   != distinto

if [ "$a" != "$b" ]

# MENOS DE ORDEN ALFABETICO

if [[ "$a" < "$b" ]]
if [ "$a" \< "$b" ]

# OPERADORES DE CADENAS

# string ... si es no nulo es verdadero

if [ cadena ]

# -z --> cadena longuitud cero

String=''   # Variable de longitud cero (null)
if [ -z "$String" ]
then
  echo "\$String está vacía."
else
  echo "\$String no está vacía."
fi

#  -n --> cadena longuitud mayor a 0

# = comparacion

if [ cadena1 = cadena2 ]

# !=

if [ cadena1 != cadena2 ]

# BOOLEANOS

# Y logico (and)

# -a

if [ exp1 -a exp2 ]

# sino tambien

if [[ exp1 && exp2 ]]

# O logico (or)

# -o

if [ exp1 -o exp2 ]

# sino tambien

if [[ exp1 || exp2 ]]

# solo se da a variables

 ==
 || 
 &&
# se usan para expresiones

if [exp1 -a exp2 || exp3 -o exp4 ]

# OPERADORES ARITMETICOS RELACIONALES enteros

-lt # (<)

-gt # (>)

-le # (<=)

-ge # (>=)

-eq # (==)

-ne # (!=) 

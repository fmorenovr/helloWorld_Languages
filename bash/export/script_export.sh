#!/bin/bash

echo "Este script hace llamada por export a otras variables"

myfun()
{
  echo "Hello!"
  echo $0
  echo " Los parametros pasados a myfun() son $* "
}

export -f myfun # -f comando para exportar funciones

bash script_import.sh

# alien

alien es bastante práctico para estas situaciones ya que nos permite transformar un paquete de un gestor de paquetes determinado en otro. Por ejemplo podemos pasar de un .rpm (Red Hat) a .deb (Debian) y viceversa. Las extensiones soportadas son:
* deb (Debian)
* rpm (Red Hat)
* slm (Stampede)
* tgz (Slackware)
* pkg (Solaris)

“–to-deb” o “-d” → para transformar a .deb
“–to-rpm” o “-r” → para transformar a .rpm
“–to-tgz” o “-t” → para transformar a .tgz
“–to-pkg” o “-p” → para transformar a .pkg
“–to-slp” → para transformar a .slp

alien -d pepino.rpm
# convierte a .deb el paquete pepino.rpm

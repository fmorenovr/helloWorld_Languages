# Comandos

ls

-a → Muestra todos los ficheros incluyendo algunos que 
     ordinariamente están ocultos para el usuario (aquellos 
     que comienzan por un punto). Recordemos que el fichero 
     punto . indica el directorio actual y el doble punto .. 
     el directorio padre, que contiene, al actual.
-l → Esta es la opción de lista larga: muestra toda la 
     información de cada fichero incluyendo: protecciones, 
     tamaño y fecha de creación o del último ambio 
     introducido,...
-c → Muestra ordenando por día y hora de creación.
-t → Muestra ordenando por día y hora de modificación.
-r → Muestra el directorio y lo ordena en orden inverso.
-R → Lista también subdirectorios.
ls subdir → Muestra el contenido del subdirectorio subdir.
-l filename → Muestra toda la información sobre el fichero 
              filename.
--color → Muestra el contenido del directorio coloreado.


sudo apt-get autoremove
// quita archivos temporales 
// Borra los paquetes huérfanos, o las dependencias que quedan instaladas después de haber instalado una aplicación y luego eliminarla, por lo que ya no son necesarias.

sudo apt-get autoclean

// Elimina del cache los paquetes .deb con versiones anteriores a los de los programas que tienes instalados.

sudo apt-get clean

// Elimina todos los paquetes del cache. El único inconveniente que podría resultar es que si quieres reinstalar un paquete, tienes que volver a descargarlo.


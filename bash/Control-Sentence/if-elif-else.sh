#!/bin/bash
#Test IF-ELSE
 
echo ' Adivina el valor numerico de la variable'
read A
 
if [ $A = 1 ];then
  echo 'Has acertado'
  exit 0
elif [ $A = 2 ];then
  echo 'Estuviste cerca'
else
  echo 'Erraste'
fi
 
exit 0

# siempre un if termina con un fi

#!/bin/bash

# al ejecutar 
#bash -x source.sh

#+ clear

#+ echo 'The Script starts now.'
#The Script starts now.
#+ echo 'Hi, cctic'
#Hi, cctic
#+ echo

#+ COLOUR=black
#+ VALUE=9
#+ echo 'This is a string: black'
#This is a string: black
#+ echo 'And this is a number: 9'
#And this is a number: 9
#+ echo

# parece eso, que es la salida que genera el bash

# o tambien puede hacer debugg en un bloque del script
clear
echo "The Script starts now."

echo "Hi, $USER"
echo

COLOUR="black"
VALUE="9"

set -x
echo "This is a string: $COLOUR"
echo "And this is a number: $VALUE"
echo
w
set +x

echo "Final bye bye "

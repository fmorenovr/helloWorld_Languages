#!/bin/bash

limite=5
i="10 9 8 7 6 5 4 3 2 1" 
 
for a in $i; do
  if [ "$a" -gt "$limite" ];then
    echo Acción $a ejecutada
    continue
  else
    break
  fi
done

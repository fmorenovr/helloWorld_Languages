# Get the definition of a class.

getClass(Class, .Force = FALSE, where)
getClassDef(Class, where, package, inherits = TRUE)

# Class: the character-string name of the class, often with a
#          ‘"package"’ attribute as noted below under ‘package’.

#  .Force: if ‘TRUE’, return ‘NULL’ if the class is undefined;
#          otherwise, an undefined class results in an error.

#   where: environment from which to begin the search for the
#          definition; by default, start at the top-level (global)
#          environment and proceed through the search list.

# package: the name of the package asserted to hold the definition.  If
#          it is a non-empty string it is used instead of ‘where’, as
#          the first place to look for the class.  Note that the package
#          must be loaded but need not be attached.  By default, the
#          package attribute of the ‘Class’ argument is used, if any.
#          There will usually be a package attribute if ‘Class’ comes
#          from ‘class(x)’ for some object.

# inherits: Should the class definition be retrieved from any enclosing
#          environment and also from the cache?  If ‘FALSE’ only a
#          definition in the environment ‘where’ will be returned.


getClass("numeric") ## a built in class

cld <- getClass("thisIsAnUndefinedClass", .Force = TRUE)
cld
## a NULL prototype
## If you are really curious:

utils::str(cld)
## Whereas these generate errors:
try(getClass("thisIsAnUndefinedClass"))
try(getClassDef("thisIsAnUndefinedClass"))



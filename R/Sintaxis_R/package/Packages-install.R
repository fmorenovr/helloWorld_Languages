INSTALAR PAQUETES

install.packages()

# son instaladas todas las CRAN

#que es CRAN ??

#Comprehensive R Archive Network

install.packages("slidify")

install.packages(c("slidify","ggplot2","devtools"))

# instala los 3 paquetes 

# tambien puede hacerse de este metodo

source("http://bioconductor.org/biocLite.R")

# llama la funcion biocLite desde el url anterior

biocLite()

# ahora usas la funcion biocLite

biocLite(c("GenomicFeatures","AnnotationDbi"))

#--------------------------------------

library()

# sirve para cargar los paquetes, no basta con solo 
# instalarlos

library(ggplot2)
# carga la libreria ggplot2
# y permite usar las funciones en la libreria ggplot2

.libPaths()
# nos muestra la locacion de la bibliotecas


search()

# nos da una lista de los paquetes adjuntos instalados disponibles

find.package("devtools")

# busca si tenemos el paquete devtools en nuestro sistema

rtools

install.packages("devtools")
# instala el paquete devtools

find.package("devtools")
# busca si tenemos instalado el paquete devtools

library("devtools")
# carga el paquete devtools

find_rtools() 
# devuelve TRUE porque existe rtools

# DESINSTALAR PAQUETES

remove.packages("graphics",'C:/Users/Felipe Moreno/Documents/R/win-library/3.1/...")

# desinstala y elimina el paquete de nombre "graphics" desde su direccion de directorio


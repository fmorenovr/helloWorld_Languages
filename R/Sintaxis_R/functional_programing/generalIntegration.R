source("Numerical_Integration.R")

# una funcion general

composite <- function(f, a, b, n = 10, rule) {
  points <- seq(a, b, length = n + 1)

  area <- 0
  for (i in seq_len(n)) {
    area <- area + rule(f, points[i], points[i + 1])
  }

  area
}

composite(sin,0,pi, n=10,rule = rectangule)
#> [1] 1.983524
composite(sin, 0, pi, n = 10, rule = midpoint)
#> [1] 2.008248
composite(sin, 0, pi, n = 10, rule = trapezoid)
#> [1] 1.983524
composite(sin, 0, pi, n = 10, rule = simpson)
#> [1] 2.000007
composite(sin, 0, pi, n = 10, rule = boole)
#> [1] 2

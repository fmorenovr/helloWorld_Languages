# esta funcion cra lineas de codigo html

simple_tag <- function(tag) {
  force(tag)
  function(...) {
    paste0("<", tag, ">", paste0(...), "</", tag, ">")
  }
}

simple_tag("p")("Hola soy Meow")
#> [1] "<p>Hola soy Meow</p>"


tags <- c("p", "b", "i")


# nombres <- setNames(tags,tags)
# nombres
#>   p   b   i 
#> "p" "b" "i" 
# nombres[1]
#>   p
#> "p"
# nombres[[1]]
#> "p"

html <- lapply(setNames(tags, tags), simple_tag)
# a cada miembro de p, b, i le asigna la funcion simple_tag
# es decir le asigna a html una lista de p, b, i
# donde cada uno es una funcion cuyo parametro tag es "p", "b", "i"
# respectivamente

html$p("This is ", html$b("bold"), " text.")
# html$p llama a la funcion con tag + p
# y asi sucesivamente



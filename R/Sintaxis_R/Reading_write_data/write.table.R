# write.table(...) function writes a matrix or data frame into a file. It's usage is:

write.table(x,file="",append=FALSE,quote=TRUE,sep=" ",eol="\n",na="NA", dec=".",row.names=TRUE,col.names=TRUE,qmethod=c("escape", "double"),fileEncoding = "")

write.csv(x, file = "", append = FALSE, quote = TRUE, sep = " ",eol = "\n", na = "NA", dec = ".", row.names = TRUE,col.names = TRUE, qmethod = c("escape", "double"),fileEncoding = "")

write.csv2(...)


# x: The matrix or data frame to be written
# file: Out file name, if empty, then print to the screen
# append: Append model, otherwise new file will be created
# sep: Seperator, default is " "
# ...

x <- matrix(c(3,5,7,1,9,4),nrow=3,ncol=2,byrow=TRUE)
write.table(x,"")

"V1" "V2"
"1" 3 5
"2" 7 1
"3" 9 4

write.table(x,"test.csv",sep=",")

# The content of "test.csv" is:

"V1","V2"
"1",3,5
"2",7,1
"3",9,4


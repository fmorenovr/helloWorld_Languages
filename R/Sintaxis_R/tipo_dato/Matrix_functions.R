### Single vale decomposition

#svd()

svd(x, nu = min(n, p), nv = min(n, p), LINPACK = FALSE)

La.svd(x, nu = min(n, p), nv = min(n, p))

# x: a numeric, logical or complex matrix

# nu: the number of left singular vectors to be computed. This must between 0 and n = nrow(x)

# nv: the number of right singular vectors to be computed. This must be between 0 and p = ncol(x)

# LINPACK: logical. Should LINPACK be used (for compatibility with R < 1.7.0)? In this case nu must be 0, nrow(x) or ncol(x)

x <- matrix(1:16,4,4)

svd(x)

### Choleski factorization

# solo sirve para matrices simetricas

#chol()

x <- matrix(c(8,1,1,4),2,2)
 
chol(x)

### Crossprod

# producto punto de matrices
# AxB

crossprod(x, y = NULL)
tcrossprod(x, y = NULL)

x <- matrix(1:9,3,3)

crossprod(x)

tcrossprod(x)

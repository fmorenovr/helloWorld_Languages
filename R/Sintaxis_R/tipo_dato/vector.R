# VECTORES

v<-c(1,2,3,4,5)

v<-vector("numeric",length=10)

v<-1:4 # crea numeros del 1 al 4

bool <- c(TRUE,FALSE,TRUE,FALSE)

bool <- c(T,F)

nombre<-c("Pedro","Jose")

assign("edad",c(20,12,21,34))
# asigna a la variable edad los valores de c()

f<-seq(0,60,10)
# fomr=0, to=60 , by=10

y <- c(1.7, "a") ## character 
y <- c(TRUE, 2) ## numeric
y <- c("a", TRUE) ## character

rep(1,5)
# sale "1" 5 veces

rep(1:3,times=1:3)
# crea 1 2 2 3 3 3

rep(1:2,each=2)
# 1 1 2 2

rep(c("a","b","c"),2)
# a b c a b c



class(y)
# devuelve el tipo de valor

as.numeric(y)

as.logical(y)

as.character(y)

as.complex(y)


is.na() # evalua si es un NA

is.nan() # evalua si es NaN

x<-c(1,2,NA,NaN)

is.na(x)
is.nan(x)


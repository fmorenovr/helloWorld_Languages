# ejemplo de trycatch

tryCatch({
  doStuff()
  doMoreStuff()
}, some_exception = function(se) {
  recover(se)
})

# ejemplo de try catch en java

try {
  doStuff();
  doMoreStuff();
} catch (SomeException se) {
  recover(se);
}



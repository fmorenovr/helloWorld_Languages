
#funcion definida
x <- 10
f1 <- function(x) {
  function() {
    x + 10
  }
}
f1(1)()


# Forma polaca reducida
`+`(1, `*`(2, 3))



#the body(), the code inside the function.

#the formals(), the list of arguments which controls how you can call the function.

#the environment(), the “map” of the location of the function’s variables.


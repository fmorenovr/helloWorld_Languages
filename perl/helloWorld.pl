#!/usr/bin/env perl
use strict;
use warnings;

sub main{
  my @argv = @_;
  my $argc = (@argv);
  print("\n\tHola Mundo Perl !!\n\n");
  print("Numero de argumentos: ".($argc+1)."\n");
  print("Argumento[0] = $0\n");
  foreach my $i(0..($argc-1)){
    print("Argumento[".($i+1)."] = $argv[$i]\n");
  }
  return;
}

main(@ARGV);

# run the file:
# perl helloWorld.pl

#!/usr/bin/env php

<?php
class HelloWorld{
  public static function main($arguments, $count){
    print("\tHola Mundo PHP !!\n\n");
    print("Numero de argumentos: $count\n");
    foreach($arguments as $key => $value){
      print("Argumento[$key] = $value\n");
    }
    return;
  }
}
HelloWorld::main($argv, $argc);
// run the file:
# php helloWorld.php
?>

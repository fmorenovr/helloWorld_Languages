package main

import (
  "os";
  "fmt";
)

func main() {
  fmt.Printf("\n\tHola Mundo Go !!\n\n");
  fmt.Printf("Numero de argumentos: %d\n", len(os.Args));
  for i,e := range os.Args {
    fmt.Printf("Argumento[%d] = %s\n", i, e);
  }
  return ;
}

// run the file:
// go build -o helloWorld helloWorld.go
// ./helloWorld

package main

import (
    "fmt"
    MQTT "git.eclipse.org/gitroot/paho/org.eclipse.paho.mqtt.golang.git"
    "strconv"
    "strings"
    "time"
)

func main() {
    //fmt.Printf("You say: %v\n", <-c)
    const n = 10000
    for i := 0; i < n; i++ {
        c := make(chan string)
        c2 := make(chan string)
        go bob_recv(42, c)
        go alice_send(42, "bob", "hey bob",c2 )
        timeout := time.After(1 * time.Second)
        for {
            select {
            case s := <-c:
                save_data("received")
                continue
            case s2 := <-c2:
                save_data("sent")
                continue
            case <-timeout:
                save_data("timeout")
                break
            }
        }
    }
}

func alice_send(id int, buddy_name string, msg string, c chan string) {
    client := connect_mqtt(id, "alice")
    room := "stress"
    pub_topic := strings.Join([]string{"/test/", room, "/", buddy_name}, "")
    fmt.Println("Printing " + pub_topic + "\n")
    message := msg
    r := client.Publish(MQTT.QOS_TWO, pub_topic, strings.TrimSpace(message))
    <-r
    c <- fmt.Sprintf("Send: %s", message)
}


func handle_msg(client *MQTT.MqttClient, msg MQTT.Message) {
    msg_from := strings.Split(msg.Topic(), "/")[3]
    fmt.Println(msg_from + "RECEIVED!!!: " + string(msg.Payload()))
}

func bob_recv(id int, c chan string) {
    client := connect_mqtt(id, "bob")

    room := "stress"
    sub_topic := strings.Join([]string{"/test/", room, "/+"}, "")
    filter, _ := MQTT.NewTopicFilter(sub_topic, 1)

    client.StartSubscription(func(client *MQTT.MqttClient, msg MQTT.Message) {
        msg_from := strings.Split(msg.Topic(), "/")[3]
        c <- fmt.Sprintf("%s RECEIVED!!!: %s", msg_from, string(msg.Payload()))
    }, filter)

}

func connect_mqtt(id int, user string) (client *MQTT.MqttClient) {

    server := "tcp://localhost:1883"
    name := user + strconv.Itoa(id)

    opts := MQTT.NewClientOptions().AddBroker(server).SetClientId(name).SetCleanSession(true)
    client = MQTT.NewClient(opts)
    _, err := client.Start()
    if err != nil {
        panic(err)
    } else {
        fmt.Printf("Connected as %s to %s\n", name, server)
    }

    return client

}

class HelloWorld {
  public static void main(String[] args) {
    int argc = args.length;
    System.out.println("\n\tHola Mundo Java !!\n");
    System.out.printf("Numero de argumentos: %d\n", argc+1);
    System.out.printf("Argumento[0] = %s\n", "nombre del archivo");
    for(int i =0; i<argc; i++) {
      System.out.printf("Argumento[%d] = %s\n", i+1, args[i]);
    }
  }
}

// run the file:
// javac helloWorld.java
// java HelloWorld

/*Modificadores

Public: es visto y accedido por cualquier clase 

private: solo en su misma clase

El nombre de la clase, con la letra inicial en mayúsculas por convención.

la primera palabra (o la única) en un nombre de método debe ser un verbo.


Modifier	Class	Package	Subclass	World
public	    Y 	   Y	     Y	      Y
protected	  Y   	 Y	     Y	      N
no modifier	Y	     Y     	 N      	N
private	    Y   	 N	     N  	    N

la palabra static : 
significa que es una variable que no cambia su contenido y que es variante para todas las instancias de clase
 es decir, si tengo 2 objetos A y B, y na variable public static int cont=0 si le digo A.cont ++ entonces cont sera 1 para A y B y si escribo B.coun-- , count sera 0 para A y B
y ademas se puede acceder a sus variables o methodos sin necesidad de instanciar un objeto de la clase


la palabra final:
es una variable que no se puede modificar por nada
double final PI = 3.14; cuando intente modificarlasera error

public class IdentifyMyParts {
    public static int x = 7;
    public int y = 3;
} 
Question: What are the class variables?

Answer: x

Question: What are the instance variables?

Answer: y

una clase local( interna ) puede acceder a las variables final o no final del bloque envolvente, por lo general estan dentro de un methodo

una clase local no ppuede definir  declarar miembros staticos y es una declaracion de clase

Clases locales son no estático porque no tienen acceso a los miembros de instancias del bloque que lo contiene. En consecuencia, no pueden contener la mayoría de los tipos de declaraciones estáticas.

una Interface no puede ser declarada en un bloque porque son inherentemente estaticas

Además, una clase local tiene acceso a las variables locales. Sin embargo, una clase local sólo se puede acceder a las variables locales que se declaran final. Cuando una clase local tiene acceso a una variable o parámetro del bloque que lo contiene local, se capta esa variable o parámetro

Las clases locales que estan dentro de un metodo ESTATICO, solo pueden acceder a las variables ESTATICAS del metodo contenedor

*/

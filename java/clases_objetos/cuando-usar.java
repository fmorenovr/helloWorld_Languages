Clase Local : Úselo si necesita crear más de una instancia de una clase, el acceso a su constructor, o introducir un nuevo tipo con nombre (porque, por ejemplo, es necesario invocar métodos adicionales más adelante).

Clase Anonymous : Úselo si necesita declarar campos o métodos adicionales.

Lambda expresión :

Úsalo si está encapsulando una sola unidad de comportamiento que desea pasar a otro código. Por ejemplo, se utiliza una expresión lambda si quieres una cierta acción realizada en cada elemento de una colección, cuando se termina un proceso, o cuando un proceso encuentra un error.

Úselo si necesita un ejemplo simple de una interfaz funcional y ninguno de los criterios anteriores de aplicar (por ejemplo, usted no necesita un constructor, un tipo con nombre, campos o métodos adicionales).

Clase anidada : Úsalo si sus requisitos son similares a los de una clase local, desea hacer el tipo más ampliamente disponible, y no se requiere acceso a variables locales o parámetros del método.

Utilice una clase anidada no estático (o clase interna) si necesita acceso a los campos y métodos no públicos de una instancia que encierra. Utilice una clase anidada estática si no se requiere este acceso.

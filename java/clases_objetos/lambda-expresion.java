/*
cuando se tiene una interfaz funcional  ( significa que solo contiene un metodo abstracto ) ( o tambien puede tener uno o mas metodos por defecto o metodos estaticos )

public class Person {

    public enum Sex {
        MALE, FEMALE
    }

    String name;
    LocalDate birthday;
    Sex gender;
    String emailAddress;

    public int getAge() {
        // ...
    }

    public void printPerson() {
        // ...
    }
}

metodo para imprimir personas con ciertos parametros

public static void printPersonsWithinAgeRange(
    List<Person> roster, int low, int high) {
    for (Person p : roster) {
        if (low <= p.getAge() && p.getAge() < high) {
            p.printPerson();
        }
    }
}

otros metodos en otra clase que llama a Person
----------------------------------------------------------
interfaz usada por todos

interfaz CheckPerson {
      boolean test (Persona p);
 }

defino el metodo printPersons que valida con tester para imprimir los criterios

public static void printPersons(
    List<Person> roster, CheckPerson tester) {
    for (Person p : roster) {
        if (tester.test(p)) {
            p.printPerson();
        }
    }
}

metodo clase local

clase local

class CheckPersonEligibleForSelectiveService implements CheckPerson {
    public boolean test(Person p) {
        return p.gender == Person.Sex.MALE &&
            p.getAge() >= 18 &&
            p.getAge() <= 25;
    }
}
que contiene un metodo que retorna true o false a los parametros indicados

metodo printPersons lo llamo
printPersons(roster, new CheckPersonEligibleForSelectiveService());


metodo clase anonima

clase anonima

en la misma llamada a la funcion estoy definiendo mi clase que usa la interfaz test CheckPerson

printPersons (
     lista,
     new CheckPerson () {
         public boolean test (Persona p) {
             p.getGender volver () == Person.Sex.MALE
                 && P.getAge ()> = 18
                 && P.getAge () <= 25;
         }
     }
 );
 

metodo expresion lambda

expersion lambda

en la misma llamada a la funcion ya definida, le pasa por parametro
a una expresion que retorna como true o false

printPersons (
     lista,
     (Persona p) -> p.getGender () == Person.Sex.MALE 
          && P.getAge ()> = 18 
          && P.getAge () <= 25
 );

otra forma de usarlo

interface Predicate<Person> {
    boolean test(Person t);
}

public static void printPersonsWithPredicate(
    List<Person> roster, Predicate<Person> tester) {
    for (Person p : roster) {
        if (tester.test(p)) {
            p.printPerson();
        }
    }
}

printPersonsWithPredicate(
    roster,
    p -> p.getGender() == Person.Sex.MALE
        && p.getAge() >= 18
        && p.getAge() <= 25
);

en una expresion lambda se puede omitir el tipo de dato.
y se usa con -> similar a un puntero 

Un cuerpo, que consta de una sola expresión o un bloque de declaración. Este ejemplo utiliza la siguiente expresión:

p.getGender() == Person.Sex.MALE 
    && p.getAge() >= 18
    && p.getAge() <= 25
    
Si especifica una sola expresión, entonces el tiempo de ejecución de Java evalúa la expresión y devuelve su valor. Alternativamente, puede utilizar una instrucción return:

p->{
     p.getGender volver () == Person.Sex.MALE
         && P.getAge ()> = 18
         && P.getAge () <= 25;
}
*/

class Calculator {
  
    interface IntegerMath {
        int operation(int a, int b);   
    }
  
    public int operateBinary(int a, int b, IntegerMath op) {
        return op.operation(a, b);
    }
 
    public static void main(String... args) {
    
        Calculator myApp = new Calculator();
        IntegerMath addition = (a, b) -> a + b;
        IntegerMath subtraction = (a, b) -> a - b;
        System.out.println("40 + 2 = " +
            myApp.operateBinary(40, 2, addition));
        System.out.println("20 - 10 = " +
            myApp.operateBinary(20, 10, subtraction));    
    }
}
/*
java.util.function.Consumer importación;

 public class LambdaScopeTest {

     public int x = 0;

     clase FirstLevel {

         public int x = 1;

         anular methodInFirstLevel (int x) {
            
             // La siguiente declaración hace que el compilador para generar
             // El error "variables locales referenciado desde una expresión lambda
             // Debe ser final o con eficacia final "en la declaración de A:
             //
             // X = 99;
            
             Consumidor <Integer> myConsumer = (y) -> 
             {
                 System.out.println ("x =" + x);  Declaración // A
                 System.out.println ("y =" + y);
                 System.out.println ("this.x =" + this.x);
                 System.out.println ("LambdaScopeTest.this.x =" +
                     LambdaScopeTest.this.x);
             };

             myConsumer.accept (x);

         }
     }

     public static void main (String ... args) {
         LambdaScopeTest st = new LambdaScopeTest ();
         LambdaScopeTest.FirstLevel fl = st.new FirstLevel ();
         fl.methodInFirstLevel (23);
     }
 }

  x = 23
  y = 23
  this.x = 1
  LambdaScopeTest.this.x = 0 
  
  Si sustituye el parámetro x en lugar de y en la declaración de la expresión lambda myConsumer , el compilador genera un error:

  Consumidor <Integer> myConsumer = (x) -> {
     // ...
 } 
El compilador genera el error "variable x ya está definido en el método methodInFirstLevel (int)" porque la expresión lambda no introduce un nuevo nivel de alcance. En consecuencia, se puede acceder directamente a los campos, métodos y variables locales del ámbito circundante. Por ejemplo, la expresión lambda accede directamente el parámetro x del método methodInFirstLevel . Para acceder a las variables de la clase envolvente, utilice la palabra clave this . En este ejemplo, this.x se refiere a la variable miembro FirstLevel.x .

Sin embargo, al igual que las clases locales y anónimas, una expresión lambda sólo puede acceder a las variables y parámetros del bloque que lo contiene que son finales o con eficacia definitiva locales. Por ejemplo, supongamos que se agrega el siguiente instrucción de asignación inmediatamente después de la methodInFirstLevel sentencia de definición:

  anular methodInFirstLevel (int x) {
     x = 99;
     // ...
 } 
Debido a esta instrucción de asignación, la variable FirstLevel.x no es realmente definitiva más. Como resultado, el compilador Java genera un mensaje de error similar a "variables locales que se hace referencia de una expresión lambda debe ser final o con eficacia final" donde la expresión lambda myConsumer intenta acceder a la FirstLevel.x variables:

  System.out.println ("x =" + x); 
  
supongase que se tiene:

public interface Runnable {
    void run();
}

public interface Callable<V> {
    V call();
}

Suponga que ha sobrecargado el método invoke de la siguiente manera:

void invoke(Runnable r) {
    r.run();
}

<T> T invoke(Callable<T> c) {
    return c.call();
}

¿Qué método se invocará en la siguiente declaración?

String s = invoke(() -> "done");

El método invoke(Callable<T>) será invocado porque ese método devuelve un valor; el método invoke(Runnable) no lo hace. En este caso, el tipo de la expresión lambda () -> "done" es Callable<T> .

*/

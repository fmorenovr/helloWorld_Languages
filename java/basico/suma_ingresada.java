import java.util.Scanner;

class SumaIngresada {
  public static void main(String[] args)
  {
    Scanner dato = new Scanner(System.in);
    int suma=0,j=10,numero;
    for(int i=0;i<j;i++)
    {
      numero=dato.nextInt(); // la funcion nextXxx esos Xxx son un tipo de dato
      // nextLine es un string
      suma = suma+numero;
    }  
    System.out.println("La suma de los "+(j)+" primeros numeros es: "+suma);
  }
}

/*
import java.io.*; // para la clase BufferedReader

class SumaIngresada {
  public static void main(String[] args) throws IOException
  {
    BufferedReader dato = new BufferedReader(new InputStreamReader(System.in));
    int suma=0,numero,j=10;
    for(int i=0;i<10;i++)
    {
      numero =Integer.parseInt(dato.readLine()); // read line viene de IOException
      suma = suma+numero;
    }  
    System.out.println("La suma de los "+j+" primeros numeros es: "+suma);
  }
}
*/

## NodeJS

Interprete de codigo javascript, además tambien instalará npm.

    curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
    nano nodesource_setup.sh
    sudo bash nodesource_setup.sh
    sudo apt-get install nodejs
    sudo apt-get install npm
    sudo npm install -g bower

## Programs

To run an js file:

    nodejs file.js

## ReactJS

Librería de javascript de Server-Side para el mantenimiento de una SPA (Simple Page Application). Embebe html en javascript, funciona en el DOM virtual.

    sudo npm install -g create-react-app
    create-react-app mywebapp
    cd mywebapp/
    npm start

## VueJS

Es un framework para contruir user interfaces SPA (simple-page applications) usando toolkits y librerias.

    npm install --global vue-cli
    vue init webpack my-project
    cd my-project
    npm install
    npm run dev

or tambien:
    
    npm install vue

## AngularJS

Framework de javascript de Client-Side para el mantenimiento de una SPA (Simple Page Application). Embebe javascript en html, funciona en el DOM real.  
Instala tus modulos usando:

    npm install packagename
    bower install packagename

## Angular2

Framework de javascript de Client-Side para el mantenimiento de una SPA (Simple Page Application). Transpila tyscript y Embebe javascript en html, funciona en el DOM real.  
Instala tus modulos usando:

    npm install -g @angular/cli
    ng new my-app
    cd my-app
    ng serve --open

## MEAN

### Mongo driver

Sirve para las conexiones a una base de datos MongoDB

    npm install mongodb --save 

### ExpressJS

Libreria que proporciona aplicaciones Server-Side en javascript.

    npm install express --save

### nodemon

Sirve para que cada vez que modifiquemos un archivo, se actualice

    npm install nodemon --save-dev

ejecutar:

    ./node_modules/.bin/nodemon server.js

### body parser

Sirve para que pueda leer informacion desde los templates

    npm install body-parser --save

### ejs

Sirve para el dinamismo de los templates html

    npm install ejs --save

### bower

Sirve para instalar los componentes (como boostrap, angular, etc) en documento bower.json:

Usando sudo:

    npm install -g bower

Cuando tengas componentes de bower.json:

    bower install


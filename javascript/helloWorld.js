#!/usr/bin/env nodejs

var path = require('path');

function main(argv){
  var argc = argv.length;
  console.log('\n\tHola Mundo Javascript !!\n');
  console.log(`Numero de argumentos: ${(argv.length-1)}`);
  console.log('Argumento[0] = ' + path.basename(argv[1]))
  for (i=2; i< argc; i++){
    console.log(`Argumento[${i-1}] = ${argv[i]}`);
  }
  return;
}

main(process.argv)

// run the file:
// nodejs helloWorld.js

choices = ['pizza', 'pasta', 'salad', 'nachos']

print 'Your choices are:'
for index, item in enumerate(choices): # el orden es ... Index a los indices e items a los elemtnso, debido a que enumerate crea los indices :D
    print index+1, item;





""" la funcion enumerar en este ejemplo por ejemplo:
		nuestra tupla es de comidas, la funcion enumerar la transforma en:


list(enumerate(choice)) ====> [(0, 'pizza'), (1, 'pasta'), (2, 'salad'), (3, 'nachos')]


list(enumerate(choice, start=1)) ===== >    [(1, 'pizza'), (2, 'pasta'), (3, 'salad'), (4, 'nachos')]


"""


list_a = [3, 9, 17, 15, 19]
list_b = [2, 4, 8, 10, 30, 40, 50, 60, 70, 80, 90]

for a, b in zip(list_a, list_b): # aqui a pertenece a lista_a y b a lista_b, todo sigue su orden
    if(a>b):
        print a;
    elif(b>a):
        print b;
    else:
        print a;


""" La funcion zip sirve para agrupar los primeros elementos de las listas con os prieros de otras listas segun el ejemplo tambien pudo haber sido:


lista_c = zip(list_a,list_b)



Anexo: Uso de la funcion Range:

>> [i for i in range(4)]
[0, 1, 2, 3]

>> [i for i in range(2, 8)]
[2, 3, 4, 5, 6, 7]

>> [i for i in range(2, 13, 3)]
[2, 5, 8, 11]

"""

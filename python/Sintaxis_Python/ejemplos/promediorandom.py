import random

i=1

n=0

promema=0 # promedio de los de mañana

prometa=0 # promedio de los de tarde

promeno=0 # promedio de los de noche

print " Turnos y edades "

print "Alumno   Manana  Tarde  Noche"

while(i<41):
    if(i<=25):  # para los primeros 25 alumnos de la mañana
        x = random.randint(15,25)
        y = random.randint(15,25)
        z = random.randint(15,25)
        if(i<10): # solo para imprimirlo con orden 
            print "0"+str(i) + "       " + str(x) + "        " + str(y) + "        " + str(z)
        else:
            print str(i) + "       " + str(x) + "        " + str(y) + "        " + str(z)
        promema = promema + x
        prometa = prometa + y
        promeno = promeno + z

    elif(25<i<=30): # seguira haciendolo con los de la tarde y noche
        y = random.randint(15,25)
        z = random.randint(15,25)
        print str(i) + "         " + "        " + str(y) + "        " + str(z)
        prometa = prometa + y
        promeno = promeno + z
        
    else: # y finalmente termina de rellenar los de la noche
        z = random.randint(15,25)
        print str(i) + "                           " + str(z)
        promeno = promeno + z

    i+=1
    
# aqui acaba el while

promema = promema/25

prometa = prometa/30

promeno = promeno/40

# sacando promedio de edades de cada turno

print "El promedio de la manana es: %.2f " % promema

print "El promedio de la tarde es: %.2f " % prometa

print "El promedio de la noche es: %.2f " % promeno

# determinamos el mayor promedio

if(promema==promeno and promeno == prometa):
    print " Todos los promedios son iguales"

elif(promema<prometa):
    if(prometa<promeno):
        print " El promedio del turno noche es mayor"
    else:
        print " El promedio del turno tarde es mayor"
else:
    if(promeno<promema):
        print " El promedio del turno manana es mayor"
    else:
        print " El promedio del turno noche es mayor"

import random

# creare una funcion "generador" para crear unos 200 espacios en mi lista

def generador(n):
    num, nums = 0, []
    while num < n:
        nums.append(num)
        num += 1
    return nums

# esta lista sera la que almacene tooodos nuestros 200 numeros :D

lista = [x for x in generador(200)]

# con este while le dare los valores random usando la funcion random.randint

contador = 0

while(contador < 200):
    lista[contador] = random.randint(-99,99)
    contador = contador+1

# definimos las variables (asi se definen)

positivos, negativos, ceros =0, 0, 0
sumaposi, sumanega = 0, 0
promedio = 0
contador1 = 0

while(contador1 < 200):
    
    if(lista[contador1]<0):
        
        sumanega = sumanega + lista[contador1]; # con esto hacemos la suma de los numeros negativos, es decir, cada vez que encuentre un negativo, lo sumara y aparte hara el conteo de numeros negativos.
        negativos = negativos + 1; # con esto contamos los negativos
    elif(lista[contador1]>0):
        sumaposi = sumaposi + lista[contador1];
        positivos = positivos + 1; # con esto la cantidad de los numeros positivos
    else:
        ceros +=1; # y con este contamos los ceros

    promedio = promedio + lista[contador1];
    contador1 = contador1 + 1;
    
promedio = promedio/len(lista);   

# aqui imprimiremos nuestros numeros random :D
contador2 = 0

while(contador2 < 200):
		print "El numero en el lugar %i es: %i" %(contador2+1,lista[contador2]);
		contador2 = contador2 + 1;

print "La suma de todos los numeros positivos es: " + str(sumaposi);

print "La suma de todos los numeros negativos es: " + str(sumanega);

print " La cantidad de numeros positivos es: " + str(positivos);

print "La cantidad de numeros negativos es: " + str(negativos);

print "La cantidad de Ceros es: " + str(ceros);

print "El promedio de todos los numeros es: " + str(promedio);

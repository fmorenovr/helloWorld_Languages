#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

def main(argv):
  print("\n\tHola Mundo Python !!\n");
  argc = len(sys.argv);
  print("Numero de argumentos: %i" %(argc));
  for i in range(argc):
    print("Argumento[%i] = %s" %(i,sys.argv[i]));
  return;

if __name__ == "__main__":
  main(sys.argv[1:]);

# run the file:
# python helloWorld.py

'''
Para interpretar un solo programa

PARA LEER DATOS DESDE TECLADO, SIEMPRE DEBE ESTAR AL INICO ESTA LINEA

# -*- encoding: utf-8 -*-


0. UN SOLO PROGRAMA (MODULO)

python nombre_archivo.py

para scripts, al final debe ir 

if __name__ == '__main__':
    main()
    
ya que cuando se ejecuta como script solo, reconoce que es un script solo y cuando es importado, reconoce que ya no es main


1. VARIOS PROGRAMAS EN UNA MISMA CARPETA

en el programa que contiene el main 
imrpotar el nombre de los demas modulos sin el ".py"

python archiv_nombre.py

y en las lineas de archiv contiene
import programa1
imrpot programa2 ...

2. VARIOS PROGRAMAS EN DISTINTAS CARPETAS

Creacion de Paquetes
un paquete es una carpeta qeu contiene archivos .py
pero para que una carpeta sea considerada paquete debe tener
un archivo que sea __init__.py ( debe estar vacio )
o se le asigna una variable __all__=["metodo1","metodo2",...]
para importar como import nombrepaquete.*

├── modulo.py 
└── paquete 
    ├── __init__.py 
    ├── modulo1.py 
    └── subpaquete 
        ├── __init__.py 
        ├── modulo1.py 
        └── modulo2.py
        
Importando módulos enteros

El contenido de cada módulo, podrá ser utilizado a la vez, por otros módulos. Para ello, es necesario importar los módulos que se quieran utilizar. Para importar un módulo, se utiliza la instrucción import, seguida del nombre del paquete (si aplica) más el nombre del módulo (sin el .py) que se desee importar.

# -*- coding: utf-8 *-* 
 
import modulo          # importar un módulo que no pertenece a un paquete 
import paquete.modulo1 # importar un módulo que está dentro de un paquete
import paquete.subpaquete.modulo1

Namespaces

Para acceder (desde el módulo donde se realizó la importación), a cualquier elemento del módulo importado, se realiza mediante el namespace, seguido de un punto (.) y el nombre del elemento que se desee obtener. En Python, un namespace, es el nombre que se ha indicado luego de la palabra import, es decir la ruta (namespace) del módulo:

print modulo.CONSTANTE_1
print paquete.modulo1.CONSTANTE_1
print paquete.subpaquete.modulo1.CONSTANTE_1

Alias

Es posible también, abreviar los namespaces mediante un alias. Para ello, durante la importación, se asigna la palabra clave as seguida del alias con el cuál nos referiremos en el futuro a ese namespace importado:

import modulo as m
import paquete.modulo1 as pm
import paquete.subpaquete.modulo1 as psm

Luego, para acceder a cualquier elemento de los módulos importados, el namespace utilizado será el alias indicado durante la importación:

print m.CONSTANTE _1
print pm.CONSTANTE _1
print psm.CONSTANTE_1



Importar módulos sin utilizar namespaces

En Python, es posible también, importar de un módulo solo los elementos que se desee utilizar. Para ello se utiliza la instrucción from seguida del namespace, más la instrucción import seguida del elemento que se desee importar:

from paquete.modulo1 import CONSTANTE_1

En este caso, se accederá directamente al elemento, sin recurrir a su namespace:

print CONSTANTE_1

Es posible también, importar más de un elemento en la misma instrucción. Para ello, cada elemento irá separado por una coma (,) y un espacio en blanco:

from paquete.modulo1 import CONSTANTE_1, CONSTANTE_2

Pero ¿qué sucede si los elementos importados desde módulos diferentes tienen los mismos nombres? En estos casos, habrá que prevenir fallos, utilizando alias para los elementos:

from paquete.modulo1 import CONSTANTE_1 as C1, CONSTANTE_2 as C2 
from paquete.subpaquete.modulo1 import CONSTANTE_1 as CS1, CONSTANTE_2 as CS2 
 
print C1
print C2
print CS1
print CS2

'''

using System;

namespace HelloWorld{
  public class mainClass{
    public static void Main(string[] args){
      int argc = args.Length;
      string currentFile = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
      System.Console.WriteLine("\n\tHola Mundo C# !!\n");
      System.Console.WriteLine("Numero de argumentos: {0}", argc+1);
      System.Console.WriteLine("Argumento[0] = {0}", currentFile);
      for (int i = 0; i < argc; i++){
        System.Console.WriteLine("Argumento[{0}] = {1}", i+1, args[i]);
      }
    }
  }
}

// run the file:
// mcs helloWorld.cs -out:helloWorld
// ./helloWorld

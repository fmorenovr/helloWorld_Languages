#include <stdio.h>

int main(int argc,char* argv[]){
  int i;
  printf("\n\tHola Mundo C !!\n\n");
  printf("Numero de argumentos: %i\n",argc);
  for(i=0;i<argc;i++){
    printf("Argumento[%i] = %s\n",i,argv[i]);
  }
  return 0;
}

// run the file
// gcc helloWorld.c -o helloWorld
// ./helloWorld

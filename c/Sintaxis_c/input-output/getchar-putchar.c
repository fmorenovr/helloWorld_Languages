#include <stdio.h>
int main( )
{
   int c;

   printf( "Enter a value :");
   c = getchar( );

   printf( "\nYou entered: ");
   putchar( c );

   return 0;
}

/*
La funcion int getchar(void)

lee el siguiente carácter disponible en la pantalla y lo devuelve como un entero. Esta función sólo lee de un solo carácter a la vez. Usted puede utilizar este método en el circuito en caso de que quiera leer más de los personajes de la pantalla.

la funcion putchar(int c)

pone el carácter pasado en la pantalla y devuelve el mismo carácter. Esta función pone solamente un solo carácter a la vez. Usted puede utilizar este método en el circuito en caso de que quiera mostrar más de un carácter en la pantalla.

*/

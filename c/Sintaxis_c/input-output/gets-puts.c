#include <stdio.h>
int main( )
{
   char str[100];

   printf( "Enter a value :");
   gets( str );

   printf( "\nYou entered: ");
   puts( str );

   return 0;
}

/*

la funcion char *gets(char *s)

lee una línea desde la entrada estándar(stdin) en el buffer apuntado por s hasta que una nueva línea de terminación o EOF.

la funcion The int puts(const char *s)

escribe la cadena s y un salto de línea final en stdout.

actualmente ya no es recomendado usar gets()
ya no es seguro su uso en el nuevo standar C
*/

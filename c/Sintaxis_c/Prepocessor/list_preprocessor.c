#include <stdio.h>

/* Lista de Preprocessadores C

Directive 	  Description

#define	      Substitutes a preprocessor macro
#include	    Inserts a particular header from another file
#undef	      Undefines a preprocessor macro
#ifdef	      Returns true if this macro is defined
#ifndef	      Returns true if this macro is not defined
#if	          Tests if a compile time condition is true
#else	        The alternative for #if
#elif	        #else an #if in one statement
#endif	      Ends preprocessor conditional
#error	      Prints error message on stderr
#pragma	      Issues special commands to the compiler, using a standardized method

*/

#ifndef MESSAGE // o tambien if !defined (MESSAGE)
   #define MESSAGE "You wish!"
#endif

/*
MACRO           Description

__DATE__	      The current date as a character literal in "MMM DD YYYY" format
__TIME__	      The current time as a character literal in "HH:MM:SS" format
__FILE__	      This contains the current filename as a string literal.
__LINE__	      This contains the current line number as a decimal constant.
__STDC__	      Defined as 1 when the compiler complies with the ANSI standard.

*/

#define  message_for(a, b)  \
    printf(#a " and " #b ": We love you!\n")
 
// se define una funcion preprocesada
// el backslash hace uso de que continua en la siguiente linea
// los # se usan para utiliszar los parametros como cadenas

#define tokenpaster(n) printf ("token" #n " = %d", token##n)
// los dos ## es para concatenar

#define min(X, Y)  ((X) < (Y) ? (X) : (Y))

void
main()
{
   printf("File :%s\n", __FILE__ );
   printf("Date :%s\n", __DATE__ );
   printf("Time :%s\n", __TIME__ );
   printf("Line :%d\n", __LINE__ );
   printf("ANSI :%d\n", __STDC__ );

  message_for(Carole,Debra);
  int token34 = 40;
   
  tokenpaster(34);
  return 0;
}



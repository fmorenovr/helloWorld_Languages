#include<stdio.h>
int num =  75 ;  

void display();

void main()
{
  extern int num;
    printf("nNum : %d",num);
  display();
}

void display()
{
  extern int num;
    printf("nNum : %d",num);
}

/* NOTE:

1.declarar dentro de la funcion indica que la funcion usa variable
externas

2.funciones que pertenecen a un mismo codigo, no requiere la deficiion con extern
( solo usa como por ejemplo con las cabeceras

3. si una variable es definida fuera del codigo, la declaracion
extern es obligatoria
( la llama desde otro codigo)
*/

/*

toda variable en C es allocated(asignado) y liberado en la memoria

el Storage class de una variable determina estas cosas:


1. donde la variable es almacenada

las variable declaradas con "auto" son almacenadas en la memoria principal
mientras que las variables delcaradas con "register" son almacenadas en el CPU

2. ambito de variable

le dice al compilador el bloque en donde es visible la variable
" el ambito de una variable es el bloque en donde la variable esta permitida de usar"

3. valor inicial por default

4. tiempo de vida de la variable

LifeTimeOfVariable = timeOfDeclaration - timeOfvariableDestruction


Data Type	    Initial Default Value
int	            0
char	        '\0'
float	          0
double	        0
pointer	        NULL

*/

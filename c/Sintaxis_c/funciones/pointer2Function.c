#include<stdio.h>
#include<stdlib.h>
#include<math.h>

// declara una funcion que toma como parametro una funcion
double trapezoidalRule(double(*)(double),int,int,int);

int
main()
{
	double (* f) (double x); // delcara el puntero a función f notar que los parentesis son necesarios para distinguir de (double *) f(double)
	f=sin; // lo inicializa con la direccion de memoria de una función double -> double 
	double x=(*f)(3.14);
	int i;
	double s;
	double (*g[2])(double x);  /* declaracion de un vector de funciones */
	g[0]=sin;
	g[1]=cos;
	for (i=0; i<2; i++)
  {
  	s = trapezoidalRule(g[i],0,2,1000);
  	printf("La suma de cuadrados de la funcion g[%i] es %lf\n",i,s);
  }
	printf("%.10f\n",x);
	
	double result = trapezoidalRule(sin,0,3,100); // toma por referencia la direccion de la funcion sin
	printf("%.10f\n",result);
	return 0;
}

// la funcion usa la funcion como parametro
double trapezoidalRule(double(*f)(double),int a,int b,int n)
{
	int i;
	double sum=0.0,w,h=(double)(b-a)/(double)n;
	for(i=0;i<n;i++)
	{
		sum = sum + ( (*f)(a+h*i) + (*f)(a+h*(i+1)) )*h/2;
	}
	return sum;
}

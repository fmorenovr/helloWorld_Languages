#include<stdio.h>
#include<stdlib.h>

int main(void) 
{ 

	int filas = 2;
	int columnas = 3;	
	int **x;	

	int i;	 // Recorre filas 
	int j;	 // Recorre columnas 

	// Reserva de Memoria 
	printf("Primer metodo de asignacion\n");
	x = (int **)malloc(filas*sizeof(int*)); 
	
	for (i=0;i<filas;i++) 
		x[i] = (int*)malloc(columnas*sizeof(int)); 

	// Damos Valores a la Matriz 
	x[0][0] = 1; 
	x[0][1] = 2; 
	x[0][2] = 3; 

	x[1][0] = 4; 
	x[1][1] = 5; 
	x[1][2] = 6; 

	// Dibujamos la Matriz en pantalla 
	for (i=0; i<filas; i++) 
	{ 
		printf("\n"); 
		for (j=0; j<columnas; j++) 
			printf("\t%p", &x[i][j] ); 
	} 
	printf("\n\n");
	
	printf("Segundo metodo de asignacion\n");
	x = (int **)malloc(filas*sizeof(int*)); 
	
	x[0] = (int *)malloc(columnas*sizeof(int));
  for(i=1; i < columnas; i++)
      x[i]=&x[0][columnas*i];
	// Damos Valores a la Matriz 
	x[0][0] = 1; 
	x[0][1] = 2; 
	x[0][2] = 3; 

	x[1][0] = 4; 
	x[1][1] = 5; 
	x[1][2] = 6; 

	// Dibujamos la Matriz en pantalla 
	for (i=0; i<filas; i++) 
	{ 
		printf("\n"); 
		for (j=0; j<columnas; j++) 
			printf("\t%p", &x[i][j] ); 
	} 
	printf("\n");
	return 0; 
} 


Por ejemplo, considere la función

inline int f (int n) {n = n + 2; n de retorno;}

El uso de este en el código

 int x = 10;
 cout << x << endl;
 int y = f (x);
 cout << y << endl;
 cout << x << endl;

sin la definicion inline, sería imprimir

 10
 12
 10

Pero con la definicion inline que se traducirá en

 int x = 10;
 cout << x << endl;
 x = x + 2;
 y = x;
 cout << y << endl;
 cout << x << endl;

que imprima

 10
 12
 12

Esto es exactamente lo que la función.

#include<iostream>
#include<math.h>
#include<stdlib.h>

using namespace std;

int * productoMatrizVector(int m[][10],int *);

//int * productoMatrizVector(int **,int *);

int main()
{
  srand(time(NULL));
  int m[10][10],vt[10],*v,i,j;

  cout<<"Producto de Vector y Matriz de dimension 10x10"<<endl;
  for(i=0;i<10;i++)
  {
    vt[i]=(int)(rand()%10);
    for(j=0;j<10;j++)
    {
      m[i][j]=(int)(rand()%100);
    }
  }
  cout<<"La matriz es: "<<endl;
  for(i=0;i<10;i++)
  {
    for(j=0;j<10;j++)
    {
      cout<<m[i][j]<<" ";
    }
    cout<<endl;
  }
  cout<<"El vector es: "<<endl;
  for(i=0;i<10;i++)
  {
    cout<<vt[i]<<" ";
  }
  cout<<endl;
  v=productoMatrizVector(m,vt);
  cout<<"El producto es: "<<endl;
  for(i=0;i<10;i++)
  {
    cout<<v[i]<<" ";
  }
  cout<<endl;
  return 0;
}
//int * productoMatrizVector(int **m,int*v)
int * productoMatrizVector(int m[][10],int*v)
{
  int *r = (int *)malloc(sizeof(int)*10);
  int i,j,k;
  for(k=0;k<10;k++)
  {
    r[i]=0;
  }
  for(i=0;i<10;i++)
  {
    for(j=0;j<10;j++)
    {
      r[i] = r[i] + m[i][j]*v[j];
    }
  }
  return r;
} 

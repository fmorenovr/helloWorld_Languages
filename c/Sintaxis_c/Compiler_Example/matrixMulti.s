	.file	"matrixMulti.c"
	.section	.rodata
.LC0:
	.string	"\n"
.LC1:
	.string	"Inicializando arrays..."
	.align 8
.LC4:
	.string	"El proceso demoro %.8fs de tiempo.\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$435280, %rsp
	movl	%edi, -435252(%rbp)
	movq	%rsi, -435264(%rbp)
	movl	$0, %edi
	call	time
	movl	%eax, %edi
	call	srand
	movl	$.LC0, %edi
	call	puts
	movl	$.LC1, %edi
	call	puts
	call	clock
	movq	%rax, -435224(%rbp)
	movl	$0, -435236(%rbp)
	jmp	.L2
.L5:
	movl	$0, -435232(%rbp)
	jmp	.L3
.L4:
	call	rand
	movl	%eax, %ecx
	movl	$1431655766, %edx
	movl	%ecx, %eax
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	leal	1(%rdx), %eax
	cvtsi2sd	%eax, %xmm0
	movl	-435232(%rbp), %eax
	movslq	%eax, %rcx
	movl	-435236(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movsd	%xmm0, -204800(%rbp,%rax,8)
	addl	$1, -435232(%rbp)
.L3:
	cmpl	$159, -435232(%rbp)
	jle	.L4
	addl	$1, -435236(%rbp)
.L2:
	cmpl	$159, -435236(%rbp)
	jle	.L5
	movl	$0, -435236(%rbp)
	jmp	.L6
.L9:
	movl	$0, -435232(%rbp)
	jmp	.L7
.L8:
	call	rand
	movl	%eax, %ecx
	movl	$1431655766, %edx
	movl	%ecx, %eax
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	leal	1(%rdx), %eax
	cvtsi2sd	%eax, %xmm0
	movl	-435232(%rbp), %eax
	cltq
	movl	-435236(%rbp), %edx
	movslq	%edx, %rdx
	imulq	$90, %rdx, %rdx
	addq	%rdx, %rax
	movsd	%xmm0, -435200(%rbp,%rax,8)
	addl	$1, -435232(%rbp)
.L7:
	cmpl	$89, -435232(%rbp)
	jle	.L8
	addl	$1, -435236(%rbp)
.L6:
	cmpl	$159, -435236(%rbp)
	jle	.L9
	movl	$0, -435236(%rbp)
	jmp	.L10
.L13:
	movl	$0, -435232(%rbp)
	jmp	.L11
.L12:
	movl	-435232(%rbp), %eax
	cltq
	movl	-435236(%rbp), %edx
	movslq	%edx, %rdx
	imulq	$90, %rdx, %rdx
	addq	%rax, %rdx
	movl	$0, %eax
	movq	%rax, -320000(%rbp,%rdx,8)
	addl	$1, -435232(%rbp)
.L11:
	cmpl	$89, -435232(%rbp)
	jle	.L12
	addl	$1, -435236(%rbp)
.L10:
	cmpl	$159, -435236(%rbp)
	jle	.L13
	movl	$0, -435236(%rbp)
	jmp	.L14
.L19:
	movl	$0, -435232(%rbp)
	jmp	.L15
.L18:
	movl	$0, -435228(%rbp)
	jmp	.L16
.L17:
	movl	-435232(%rbp), %eax
	cltq
	movl	-435236(%rbp), %edx
	movslq	%edx, %rdx
	imulq	$90, %rdx, %rdx
	addq	%rdx, %rax
	movsd	-320000(%rbp,%rax,8), %xmm1
	movl	-435228(%rbp), %eax
	movslq	%eax, %rcx
	movl	-435236(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movsd	-204800(%rbp,%rax,8), %xmm2
	movl	-435232(%rbp), %eax
	cltq
	movl	-435228(%rbp), %edx
	movslq	%edx, %rdx
	imulq	$90, %rdx, %rdx
	addq	%rdx, %rax
	movsd	-435200(%rbp,%rax,8), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movl	-435232(%rbp), %eax
	cltq
	movl	-435236(%rbp), %edx
	movslq	%edx, %rdx
	imulq	$90, %rdx, %rdx
	addq	%rdx, %rax
	movsd	%xmm0, -320000(%rbp,%rax,8)
	addl	$1, -435228(%rbp)
.L16:
	cmpl	$159, -435228(%rbp)
	jle	.L17
	addl	$1, -435232(%rbp)
.L15:
	cmpl	$89, -435232(%rbp)
	jle	.L18
	addl	$1, -435236(%rbp)
.L14:
	cmpl	$159, -435236(%rbp)
	jle	.L19
	call	clock
	movq	%rax, -435216(%rbp)
	movq	-435224(%rbp), %rax
	movq	-435216(%rbp), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	movsd	.LC3(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, -435208(%rbp)
	movq	-435208(%rbp), %rax
	movq	%rax, -435272(%rbp)
	movsd	-435272(%rbp), %xmm0
	movl	$.LC4, %edi
	movl	$1, %eax
	call	printf
	movl	$.LC0, %edi
	call	puts
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC3:
	.long	0
	.long	1093567616
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits

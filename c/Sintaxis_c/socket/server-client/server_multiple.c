#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread

void *connection_handler(void *);
 
int
main(int argc , char *argv[])
{
  int socket_desc , client_sock , c , *new_sock;
  // numero de sockets
  int num_sock=1;
  struct sockaddr_in server , client;
  //Create socket
  socket_desc = socket(AF_INET , SOCK_STREAM , 0);
  if (socket_desc == -1)
  {
    printf("Error creando socket");
  }
  puts("Socket creado");
  // Definimos la estructura sockaddr_in
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons( 8888 );
  //Bind - enlazamos
  if( bind(socket_desc,(struct sockaddr*)&server,sizeof(server))<0)
  {
    // si hay error imprimimos
    perror("bind fallo. Error");
    return 1;
  }
  puts("bind creado");
  //Listen - Escuchamos, sobre el socket
  listen(socket_desc , 3);
  //Aceptando conexiones entrantes
  puts("Esperando conecciones entrantes...");
  c = sizeof(struct sockaddr_in);
  //Aceptando conexiones entrantes
//  puts("Esperando conecciones entrantes...");
//  c = sizeof(struct sockaddr_in);
  // aceptamos coneccion de un cliente
  // donde cada conexion retorna 1>0
  while((client_sock = accept(socket_desc,(struct sockaddr*)&client,(socklen_t*)&c)) and num_sock>0)
  {
    printf("Client_Socket #:%d\n",client_sock);
    num_sock+=1;
    puts("Conexion aceptada");
    printf("Socket #:%d\n",num_sock-1);
    pthread_t sniffer_thread;
    new_sock = malloc(1);
    *new_sock = client_sock;
    if( pthread_create( &sniffer_thread,NULL,connection_handler,(void*)new_sock)<0)
    {
      perror("No pudo crear el thread");
      return 1;
    }
    //Unimos el thread, entonces no terminamos antes del hilo
    //pthread_join( sniffer_thread , NULL);
    puts("Handler assignado");
  }
  if (client_sock < 0)
  {
    perror("accept failed");
    return 1;
  }   
  return 0;
}
 
/*
 * Maneja la conexion para cada client
 * */
void *connection_handler(void *socket_desc)
{
  //Obtiene la descripcion del socket
  int sock = *(int*)socket_desc;
  int read_size;
  char *message , client_message[2000];
  int i = 0;
  //Envia algunos mensajes iniciales al cliente
  if(i==0){
    message = "Hola! Bienvenido al servidor\n";
    write(sock , message , strlen(message));
  }
  i+=1;
  //Recibimos mensaje del cliente
  while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
  {
    //Devolvemos mensaje al cliente
    write(sock , client_message , strlen(client_message)-1);
  }
  if(read_size == 0)
  {
    puts("El client se desconecto");
    fflush(stdout);
  }
  else if(read_size == -1)
  {
    perror("recv fallo");
  }
  //Free el puntero a socket
  free(socket_desc);
  return 0;
}

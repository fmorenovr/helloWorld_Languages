// problema 3-punteros
// manejo de sizeof
#include<stdio.h>
#include<stdlib.h>
int main(){
      int *ptr,nro,i;  //declaracion de la variable puntero
      printf("sizeof de char %ld\n",sizeof(char));
      printf("sizeof de int %ld\n",sizeof(int));
      
      printf("Numero de elementos de v: ");
      scanf("%d",&nro);
      //reservando memoria para ptr
      ptr=malloc( nro*sizeof(int) );
      for(i=0;i<nro;i++){
        printf("v[%d]= ",(i+1));
        scanf("%d",&ptr[i]);
       }
       printf("Elementos del vector v:\n");
       for(i=0;i<nro;i++)
        printf("%d ",ptr[i]);
       free(ptr);  //liberar el espacio de memoria ocupado por el vector v
     
       
       return 0;
       }

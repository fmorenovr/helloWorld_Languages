//uso de parametros por referencia en una funcion, problema 4
#include<stdio.h>
#include<stdlib.h>
void intercambia(int *x,int *y);
int main(){
    int a,b;  
    
    printf("a= ");scanf("%d",&a);
    printf("b= "),scanf("%d",&b);
    intercambia(&a,&b);
    printf("a= %d\n",a);
    printf("b= %d\n",b);
    return 0;
}

void intercambia(int *x,int *y){
          int aux=*x;
          *x=*y;
          *y=aux;
          }
          

//acceso a la direccion de memoria de una variable simple
#include<stdio.h>

void cambio(int a,int b)
{
  int tmp;
  tmp=a;
  a=b;
  b=tmp;
}

int main()
{
  float r=17.3;
  float *ptr;           //declaracion de la variable puntero
  ptr= &r;
  printf("El valor que almacena r: %.2f\n",r);
  printf("La direccion de memoria de r: %p\n",&r);
  
  printf("El valor que almacena ptr: %p\n",ptr);
  printf("La direccion de memoria de ptr: %p\n",&ptr); 
  printf("La indireccion ptr: %.2f\n",*ptr);
// una variable puntero aparte de almacenar una direccion de memoria, tambien se localiza en una direccion de memoria.
  r=r+5;
  *ptr=*ptr+6;
  printf("El valor que almacena r: %.2f\n",r);
  printf("El valor que almacena ptr: %.2f\n",*ptr);
  int c=4,d=5;
  printf("C es: %i y D es: %i\n",c,d);
  cambio(c,d);
  printf("C es: %i y D es: %i\n",c,d);
  
  int a=10,b=-20;
  const int *p = &a; //objeto constante y puntero variable
  //*p = 15; ERROR: el valor apuntado por p es constante. es decir SOLO LECTURA
  printf("%i\n",*p);
  printf("%p\n",&a);
  printf("%p\n",p);
  p=&b;
  printf("%p\n",p);
  printf("%i\n",*p);
  
  int i=a+b;
  char x,t;
  x = (char)(i%10 +48);
  printf("residuo: %c \n",x);

  x = (char)(i/10 +48);
  printf("division: %c \n",x);
  
  char j='0';
  
  int y = (int)(j)-48;
  printf("%i\n",y);
}

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int
main(){
  FILE *pFile;
  char line[21]; // linea que leerea del file
  char *buffer; // nuevo buffer donde se copiara las lineas
  char *ptr; // el se mueve en el buffer para copiar
  
  buffer = (char *) malloc(1000*sizeof(char));
  memset(buffer,0,1000*sizeof(char));
  // copiara 0 1000 veces
  ptr = buffer;
   
  pFile=fopen("nombres.txt","r");
  if(pFile){
    while(!feof(pFile)){
      fgets(line,21,pFile); // leemos una linea hasta encontrar "\n"
      if(strcmp(line,"Joe\n")!=0){ // 0 are same
        strcpy(ptr,line); // le anadimos la linea a buffer
        ptr+=strlen(line);
      }
    }
    fclose(pFile);
    pFile=fopen("nombres.txt","w");
    fprintf(pFile,"%s",buffer);
    fclose(pFile);
  } else{
    printf("Could not open the file\n");
  }
  return 0;
}

// escriba un programa para escribir una cadena en un fichero o archivo
#include <stdio.h>
int main(){
	char nomArch[31],car;
	FILE *fich;      //declaracion de un fichero
	printf("nombre de archivo:  "); gets(nomArch);
	if( (fich=fopen(nomArch,"r"))==NULL ){ // r es read (leer el archivo)
		printf("No se pudo crear fichero\n");
	return 0;
	}
	while( (car=fgetc(fich))!= EOF ){ // EOF es fin de archivo
	//	printf("%c",car); // los dos valen
		putchar(car);
	}
	printf("\n");
	fclose(fich); //cierra el fichero	
return 0;
}

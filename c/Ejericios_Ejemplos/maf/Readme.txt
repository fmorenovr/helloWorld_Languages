para ejecutar este programa hacer:

usando el header maf:

gcc mafMain.c maf.c

o

gcc -c -o maf.o maf.c
gcc mafMain.c maf.o

o

usando la libreria estatica maf

ar rcs libmaf.a maf.o
gcc mafMain.c libmaf.a
gcc -L. mafMain.c -lmaf

// primero hacemos el C

// PREPROCESSING ( EXPAND MACROS)
cpp archivo.c > archivo.i
// genera archivo.i

// COMPILATION ( FROM SOURCE CODE TO ASSEMBLY)
gcc -Wall -S hello.i
// genera archivo.s

// ASSEMBLY (FROM ASEEMBLY LANGUAGE TO MACHINE CODE)
as hello.s -o hello.o
// genera hello.o

// LINKING (CREATE THE FINAL EXECUTABLE)
gcc hello.o
// genera a.out


// primero hacemos el C++

// PREPROCESSING ( EXPAND MACROS)
cpp archivo.c > archivo.i
// genera archivo.i

// COMPILATION ( FROM SOURCE CODE TO ASSEMBLY)
g++ -Wall -S hello.i
// genera archivo.s

// ASSEMBLY (FROM ASEEMBLY LANGUAGE TO MACHINE CODE)
as hello.s -o hello.o
// genera hello.o

// LINKING (CREATE THE FINAL EXECUTABLE)
g++ hello.o
// genera a.out
